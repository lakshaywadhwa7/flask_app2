from flask import Flask
from flask_restful import reqparse, abort, Api, Resource
import pickle
from flask import Flask,jsonify,request,make_response,url_for,redirect
import numpy as np
import pandas as pd
import json
import os
import json
from incoming import datatypes, PayloadValidator
from sklearn.preprocessing import OneHotEncoder


app = Flask(__name__)
api = Api(app)


clf_path = './model_final.pkl'
with open(clf_path, 'rb') as f:
    model= pickle.load(f)

# argument parsing
parser = reqparse.RequestParser()
parser.add_argument('query')

class NumpyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


class Predict_prob(Resource):
    def __init__(self):
        pass


    def log_variable(self,variable):
        if (variable==0) or (variable is None):
            return 0
        else:
            return (np.log(variable))



    def post(self):
        uuid = request.json["uuid"]
        account_amount_added_12_24m_log = self.log_variable(request.json['account_amount_added_12_24m'])
        account_days_in_dc_12_24m_log = self.log_variable(request.json['account_days_in_dc_12_24m'])
        account_days_in_rem_12_24m_log = self.log_variable(request.json['account_days_in_rem_12_24m'])
        account_days_in_term_12_24m_log = self.log_variable(request.json['account_days_in_term_12_24m'])
        account_incoming_debt_vs_paid_0_24m = request.json['account_incoming_debt_vs_paid_0_24m']
        account_status = request.json['account_status']
        account_worst_status_0_3m=request.json['account_worst_status_0_3m']
        account_worst_status_12_24m = request.json['account_worst_status_12_24m']
        account_worst_status_3_6m = request.json['account_worst_status_3_6m']
        account_worst_status_6_12m = request.json['account_worst_status_6_12m']
        age = request.json['age']
        avg_payment_span_0_12m_log = self.log_variable(request.json['avg_payment_span_0_12m'])
        avg_payment_span_0_3m = self.log_variable(request.json['avg_payment_span_0_3m'])
        has_paid_0 = request.json['has_paid_0']
        has_paid_1 = request.json['has_paid_1']
        max_paid_inv_0_24m_log = self.log_variable(request.json['max_paid_inv_0_24m'])
        max_paid_inv_0_12m_log = self.log_variable(request.json['max_paid_inv_0_12m'])
        name_in_email = request.json['name_in_email']
        num_active_div_by_paid_inv_0_12m_log = self.log_variable(request.json['num_active_div_by_paid_inv_0_12m'])
        num_active_inv = request.json['num_active_inv']
        num_arch_dc_0_12m = request.json['num_arch_dc_0_12m']
        num_arch_dc_12_24m = request.json['num_arch_dc_12_24m']
        num_arch_ok_0_12m = request.json['num_arch_ok_0_12m']
        num_arch_ok_12_24m = request.json['num_arch_ok_12_24m']
        num_arch_rem_0_12m = request.json['num_arch_rem_0_12m']
        num_arch_written_off_0_12m = request.json['num_arch_written_off_0_12m']
        num_arch_written_off_12_24m = request.json['num_arch_written_off_12_24m']
        num_unpaid_bills = request.json['num_unpaid_bills']
        recovery_debt = request.json['recovery_debt']
        sum_capital_paid_account_0_12m_log =self.log_variable(request.json['sum_capital_paid_account_0_12m'])
        sum_capital_paid_account_12_24m_log = self.log_variable(request.json['sum_capital_paid_account_12_24m'])
        sum_paid_inv_0_12m_log = self.log_variable(request.json['sum_paid_inv_0_12m'])
        time_hours = request.json['time_hours']
        worst_status_active_inv = request.json['worst_status_active_inv']
        status_last_archived_0_24m_0 = request.json['status_last_archived_0_24m_0']
        status_last_archived_0_24m_1 = request.json['status_last_archived_0_24m_1']
        status_last_archived_0_24m_2 = request.json['status_last_archived_0_24m_2']
        status_last_archived_0_24m_3 = request.json['status_last_archived_0_24m_3']
        status_last_archived_0_24m_5 = request.json['status_last_archived_0_24m_5']
        status_2nd_last_archived_0_24m_0 = request.json['status_2nd_last_archived_0_24m_0']
        status_2nd_last_archived_0_24m_1 = request.json['status_2nd_last_archived_0_24m_1']
        status_2nd_last_archived_0_24m_2 = request.json['status_2nd_last_archived_0_24m_2']
        status_2nd_last_archived_0_24m_3 = request.json['status_2nd_last_archived_0_24m_3']
        status_2nd_last_archived_0_24m_5 = request.json['status_2nd_last_archived_0_24m_5']
        status_3rd_last_archived_0_24m_0 = request.json['status_3rd_last_archived_0_24m_0']
        status_3rd_last_archived_0_24m_1 = request.json['status_3rd_last_archived_0_24m_1']
        status_3rd_last_archived_0_24m_2 = request.json['status_3rd_last_archived_0_24m_2']
        status_3rd_last_archived_0_24m_3 = request.json['status_3rd_last_archived_0_24m_3']
        status_3rd_last_archived_0_24m_5 = request.json['status_3rd_last_archived_0_24m_5']
        status_max_archived_0_6_months_0 = request.json['status_max_archived_0_6_months_0']
        status_max_archived_0_6_months_1 = request.json['status_max_archived_0_6_months_1']
        status_max_archived_0_6_months_2 = request.json['status_max_archived_0_6_months_2']
        status_max_archived_0_6_months_3 = request.json['status_max_archived_0_6_months_3']
        status_max_archived_0_6_months_5 = request.json['status_max_archived_0_6_months_5']
        status_max_archived_0_12_months_0= request.json['status_max_archived_0_12_months_0']
        status_max_archived_0_12_months_1 = request.json['status_max_archived_0_12_months_1']
        status_max_archived_0_12_months_2 = request.json['status_max_archived_0_12_months_2']
        status_max_archived_0_12_months_3 = request.json['status_max_archived_0_12_months_3']
        status_max_archived_0_12_months_5 = request.json['status_max_archived_0_12_months_5']
        status_max_archived_0_24_months_0 = request.json['status_max_archived_0_24_months_0']
        status_max_archived_0_24_months_1 = request.json['status_max_archived_0_24_months_1']
        status_max_archived_0_24_months_2 = request.json['status_max_archived_0_24_months_2']
        status_max_archived_0_24_months_3 = request.json['status_max_archived_0_24_months_3']
        status_max_archived_0_24_months_5 = request.json['status_max_archived_0_6_months_5']
        merchant_group_Entertainment = request.json['merchant_group_Entertainment']
        merchant_group_CLothing_Shoes = request.json['merchant_group_CLothing_Shoes']
        merchant_group_Leisure_Sport_Hobby = request.json['merchant_group_Leisure_Sport_Hobby']
        merchant_group_Health_Beauty = request.json['merchant_group_Health_Beauty']
        merchant_group_Children_Products = request.json['merchant_group_Children_Products']
        merchant_group_Home_Garden = request.json['merchant_group_Home_Garden']
        merchant_group_Electronics = request.json['merchant_group_Electronics']
        merchant_group_Intagible_products = request.json['merchant_group_Intagible_products']
        merchant_group_Jewelry_Accessories = request.json['merchant_group_Jewelry_Accessories']
        merchant_group_Automative_Products = request.json['merchant_group_Automative_Products']
        merchant_group_Erotic_Materials = request.json['merchant_group_Erotic_Materials']
        merchant_group_Food_Beverage = request.json['merchant_group_Food_Beverage']










        data=pd.DataFrame([[uuid,account_amount_added_12_24m_log,
       account_days_in_dc_12_24m_log, account_days_in_rem_12_24m_log,
       account_days_in_term_12_24m_log, account_incoming_debt_vs_paid_0_24m,
       account_status,account_worst_status_0_3m,
       account_worst_status_12_24m, account_worst_status_3_6m,
       account_worst_status_6_12m, age, avg_payment_span_0_12m_log,
       avg_payment_span_0_3m,
        max_paid_inv_0_12m_log, max_paid_inv_0_24m_log,name_in_email,
       num_active_div_by_paid_inv_0_12m_log, num_active_inv,
       num_arch_dc_0_12m,num_arch_dc_12_24m, num_arch_ok_0_12m,
       num_arch_ok_12_24m,num_arch_rem_0_12m,
       num_arch_written_off_0_12m,num_arch_written_off_12_24m,
       num_unpaid_bills, recovery_debt,
       sum_capital_paid_account_0_12m_log, sum_capital_paid_account_12_24m_log,
       sum_paid_inv_0_12m_log, time_hours, worst_status_active_inv,status_last_archived_0_24m_0,
       status_last_archived_0_24m_1, status_last_archived_0_24m_2,
       status_last_archived_0_24m_3, status_last_archived_0_24m_5,
       status_2nd_last_archived_0_24m_0, status_2nd_last_archived_0_24m_1,
       status_2nd_last_archived_0_24m_2, status_2nd_last_archived_0_24m_3,
       status_2nd_last_archived_0_24m_5, status_3rd_last_archived_0_24m_0,
       status_3rd_last_archived_0_24m_1, status_3rd_last_archived_0_24m_2,
       status_3rd_last_archived_0_24m_3, status_3rd_last_archived_0_24m_5,
       status_max_archived_0_6_months_0, status_max_archived_0_6_months_1,
       status_max_archived_0_6_months_2, status_max_archived_0_6_months_3,
       status_max_archived_0_6_months_5,
       status_max_archived_0_12_months_0,
       status_max_archived_0_12_months_1,
       status_max_archived_0_12_months_2,
       status_max_archived_0_12_months_3,
       status_max_archived_0_12_months_5,
       status_max_archived_0_24_months_0,
       status_max_archived_0_24_months_1,
       status_max_archived_0_24_months_2,
       status_max_archived_0_24_months_3,
       status_max_archived_0_24_months_5,merchant_group_Entertainment,merchant_group_CLothing_Shoes,merchant_group_Leisure_Sport_Hobby,merchant_group_Health_Beauty,merchant_group_Children_Products,merchant_group_Home_Garden,merchant_group_Electronics,merchant_group_Intagible_products,merchant_group_Jewelry_Accessories,merchant_group_Automative_Products,merchant_group_Erotic_Materials,merchant_group_Food_Beverage,has_paid_0,has_paid_1]], columns=['uuid','account_amount_added_12_24m',
       'account_days_in_dc_12_24m', 'account_days_in_rem_12_24m',
       'account_days_in_term_12_24m', 'account_incoming_debt_vs_paid_0_24m',
       'account_status', 'account_worst_status_0_3m',
       'account_worst_status_12_24m', 'account_worst_status_3_6m',
       'account_worst_status_6_12m', 'age', 'avg_payment_span_0_12m',
       'avg_payment_span_0_3m',
        'max_paid_inv_0_12m', 'max_paid_inv_0_24m', 'name_in_email',
       'num_active_div_by_paid_inv_0_12m', 'num_active_inv',
       'num_arch_dc_0_12m', 'num_arch_dc_12_24m', 'num_arch_ok_0_12m',
       'num_arch_ok_12_24m', 'num_arch_rem_0_12m',
       'num_arch_written_off_0_12m', 'num_arch_written_off_12_24m',
       'num_unpaid_bills','recovery_debt',
       'sum_capital_paid_account_0_12m', 'sum_capital_paid_account_12_24m',
       'sum_paid_inv_0_12m', 'time_hours', 'worst_status_active_inv','status_last_archived_0_24m_0',
       'status_last_archived_0_24m_1', 'status_last_archived_0_24m_2',
       'status_last_archived_0_24m_3', 'status_last_archived_0_24m_5',
       'status_2nd_last_archived_0_24m_0', 'status_2nd_last_archived_0_24m_1',
       'status_2nd_last_archived_0_24m_2', 'status_2nd_last_archived_0_24m_3',
       'status_2nd_last_archived_0_24m_5', 'status_3rd_last_archived_0_24m_0',
       'status_3rd_last_archived_0_24m_1', 'status_3rd_last_archived_0_24m_2',
       'status_3rd_last_archived_0_24m_3', 'status_3rd_last_archived_0_24m_5',
       'status_max_archived_0_6_months_0', 'status_max_archived_0_6_months_1',
       'status_max_archived_0_6_months_2', 'status_max_archived_0_6_months_3',
    'status_max_archived_0_6_months_5',
       'status_max_archived_0_12_months_0',
       'status_max_archived_0_12_months_1',
       'status_max_archived_0_12_months_2',
       'status_max_archived_0_12_months_3',
       'status_max_archived_0_12_months_5',
       'status_max_archived_0_24_months_0',
       'status_max_archived_0_24_months_1',
       'status_max_archived_0_24_months_2',
       'status_max_archived_0_24_months_3',
       'status_max_archived_0_24_months_5','merchant_group_Entertainment','merchant_group_CLothing_Shoes','merchant_group_Leisure_Sport_Hobby','merchant_group_Health_Beauty','merchant_group_Children_Products','merchant_group_Home_Garden','merchant_group_Electronics','merchant_group_Intagible_products','merchant_group_Jewelry_Accessories','merchant_group_Automative_Products','merchant_group_Erotic_Materials','merchant_group_Food_Beverage','has_paid_0','has_paid_1'
                                                          ])






        data = data.drop(
            ['uuid', 'account_incoming_debt_vs_paid_0_24m', 'avg_payment_span_0_3m', 'num_arch_dc_0_12m',
             'num_arch_dc_12_24m', 'num_arch_rem_0_12m', 'num_arch_written_off_12_24m', 'num_arch_written_off_0_12m','name_in_email','account_status','account_worst_status_0_3m','account_worst_status_12_24m','account_worst_status_3_6m','account_worst_status_6_12m','worst_status_active_inv','status_max_archived_0_6_months_5'],
            axis=1)


        pred_proba = model.predict_proba(data)
        json_dump = json.dumps({'prob_not_default': pred_proba[0][0],'prob_default': pred_proba[0][1]},
                               cls=NumpyEncoder)

        return json_dump






# Setup the Api resource routing here
# Route the URL to the resource
api.add_resource(Predict_prob, '/')

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(debug=True, host='0.0.0.0', port=port)
